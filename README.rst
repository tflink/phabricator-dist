Phabricator Dist
================

These are specfiles for `Phabricator <http://phabricator.org/>`_, a collection
of open source web applications that help companies build better software.

As there are no traditional releases upstream, the packages have been written
for git snapshots from master. I've been using these packages for ove a year
now and haven't had any major problems with stability so far as long as I test
the packages before deploying them to production.

Bleeding edge packages `are available in COPR <http://copr-fe.cloud.fedoraproject.org/coprs/tflink/phabricator/>`_
and a more stable repository `is available on fedorapeople <https://repos.fedorapeople.org/repos/tflink/phabricator/>`_.

Packaging TODO
--------------

I'd like to start the process of getting these packages into the main Fedora
repositories and possibly EPEL. The major tasks left before that can happen are:

  - remove bundled libraries and replace them with packaged deps

  - package any missing deps for Fedora

  - rename and move the binaries for phabricator (symlinks in /usr/share/phabricator
    at the moment)

  - figure out how to handle the wordlist dep (license is not OK for Fedora)


I've started some packaging notes and more detailed TODO in ``fedora-packaging-notes``
