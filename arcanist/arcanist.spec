%define git_short_version_hash f89f3de
%define git_full_version_hash f89f3de65805f7f65c8082ed387e8e2572596f7a
%define date 20160407

Summary: A command line interface to Phabricator
Name: arcanist
Version: %{date}.git%{git_short_version_hash}
Release: 1%{?dist}
License: ASL 2.0
URL: http://www.phabricator.com/docs/arcanist/
Source0: https://github.com/phacility/arcanist/archive/%{git_short_version_hash}.tar.gz
BuildArch: noarch

Requires: php-common >= 5
Requires: libphutil
Requires: php-xml

%description
A command line interface to Phabricator


%prep
%setup -q -n arcanist-%{git_full_version_hash}


%build


%install
# copy arcanist to the buildroot
mkdir -p %{buildroot}%{_datadir}/%{name}
cp -a * %{buildroot}%{_datadir}/%{name}/

# symlink the bin
mkdir -p %{buildroot}%{_bindir}
pushd %{buildroot}%{_bindir}
ln -s %{_datadir}/%{name}/bin/arc
popd

%files
%{_datadir}/%{name}
%{_bindir}/arc


%changelog
* Thu Apr 07 2016 Tim Flink <tflink@fedoraproject.org> - 20160407.gitf89f3de-1
- updating to latest git

* Thu Mar 10 2016 Tim Flink <tflink@fedoraproject.org> - 20160310.git1439aaa-1
- updating to latest git

* Tue Feb 23 2016 Tim Flink <tflink@fedoraproject.org> - 20160223.gited476cf-1
- updating to latest git

* Wed Feb 17 2016 Tim Flink <tflink@fedoraproject.org> - 20160217.gited476cf-1
- updating to latest git

* Tue Feb 09 2016 Tim Flink <tflink@fedoraproject.org> - 20160209.git0553cb8-1
- updating to latest git

* Fri Jan 29 2016 Tim Flink <tflink@fedoraproject.org> - 20160129.git2412c31-1
- updating to latest git

* Fri Nov 06 2015 Tim Flink <tflink@fedoraproject.org> - 20151106.gitdbd4563-2
- updating to latest git

* Tue Sep 22 2015 Tim Flink <tflink@fedoraproject.org> - 20150922.gitb37b2c3-1
- updating to latest git

* Tue Sep 15 2015 Tim Flink <tflink@fedoraproject.org> - 20150903.gitc94e604-2
- Adding php-xml as a Require

* Wed Sep 02 2015 Tim Flink <tflink@fedoraproject.org> - 20150903.gitc94e604-1
- changing to stable branch upstream
- artificial bump of date in name to differentiate

* Wed Sep 02 2015 Tim Flink <tflink@fedoraproject.org> - 20150902.git029e5a7-1
- updating to latest git

* Fri Aug 21 2015 Tim Flink <tflink@fedoraproject.org> - 20150821.git9b8c9d2-1
- updating to latest git

* Tue Aug 11 2015 Tim Flink <tflink@fedoraproject.org> - 20150811.git423e7d2-1
- updating to latest git

* Mon Jul 27 2015 Tim Flink <tflink@fedoraproject.org> - 20150727.git5fcf7b5-1
- updating to latest git

* Mon Jul 20 2015 Tim Flink <tflink@fedoraproject.org> - 20150720.git5830243-1
- updating to latest git

* Thu Jul 16 2015 Tim Flink <tflink@fedoraproject.org> - 20150716.git5e578fb-1
- updating to latest git

* Thu Jul 09 2015 Tim Flink <tflink@fedoraproject.org> - 20150709.git999eb93-1
- updating to latest git

* Wed May 13 2015 Tim Flink <tflink@fedoraproject.org> - 20151211.git6295134-1
- updating to latest git

* Wed Feb 11 2015 Tim Flink <tflink@fedoraproject.org> - 20151211.git3f132f4-1
- updating to latest git

* Thu Dec 04 2014 Tim Flink <tflink@fedoraproject.org> - 20141204.gitb46d4ed-1
- updating to latest git

* Tue Sep 02 2014 Tim Flink <tflink@fedoraproject.org> - 20140902.gitc8f1513-1
- updating to latest git

* Mon Jul 21 2014 Tim Flink <tflink@fedoraproject.org> - 20140721.gitef18ae0-1
- updating to latest git

* Sun May 18 2014 Tim Flink <tflink@fedoraproject.org> - 20140518.git0468be3-1
- updating to latest git

* Thu Jan 23 2014 Tim Flink <tflink@fedoraproject.org> - 20140123.git2c2c566-1
- updating to latest git

* Tue Dec 10 2013 Tim Flink <tflink@fedoraproject.org> - 20131210.gite0b4eef-1
- updating to latest git

* Wed Oct 30 2013 Tim Flink <tflink@fedoraproject.org> - 20131030.gitaabbdbd-1
- Initial package
