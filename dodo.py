#! /usr/bin/env python
# encoding: utf-8

import os
import glob
import codecs
import re
import subprocess
from datetime import datetime

from doit.tools import create_folder
from doit import get_var


################################################################################
# PROJECT DEFINITIONS
# This dodo file is designed to be pretty much identical in all qadevel
# projects. This section should be the only part which differs between them.
################################################################################

PROJECTS = ['libphutil', 'arcanist', 'phabricator']
REPOS = { 'libphutil': 'https://github.com/phacility/libphutil',
          'arcanist': 'https://github.com/phacility/arcanist',
          'phabricator': 'https://github.com/phacility/phabricator'
          }
MOCKENV = 'fedora-23-x86_64'
TARGETDIST = 'fc23'
BUILDARCH = 'noarch'
LOCALREPO = 'yumrepo'
GITBRANCH = 'stable'
#COPRREPO = 'https://copr-be.cloud.fedoraproject.org/results/tflink/taskotron/fedora-21-x86_64/'


################################################################################
# UTILITY FUNCTIONS
# Functions used to generate some of the needed metadata
################################################################################

#def get_version():
#    """ Assume that the version is stored as __version__ in $APPNAME/__init__.py
#    so that it can be generically extracted
#    """
#
#    version_file = codecs.open(os.path.join(NAME, '__init__.py'), 'r').read()
#    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
#                              version_file, re.M)
#    if version_match:
#        return version_match.group(1)
#    raise RuntimeError("Unable to find version string.")

def get_version(project):
    """get version from git checkout"""
    pass

def get_rpmrelease(specfile):
    """parse the rpm release out of the specfile that we're currently using"""
    rpmspec_command = ['rpmspec', '-q', '--queryformat="%{RELEASE}\\n"', specfile]
    raw_output = subprocess.check_output(rpmspec_command).split()[0].lstrip('"')
    return raw_output.split('.')[0]


################################################################################
# GENERAL VARIABLES
# These variables are computed based on project definitions and current state
# as a convenience for later functions
################################################################################

#VERSION = get_version('none')
#TARGETNAME = "{}-{}".format(NAME, VERSION)
#SPECFILE = '{}.spec'.format(NAME)
#RPMRELEASE = get_rpmrelease(os.path.abspath(SPECFILE))
#NVR = '{}-{}-{}.{}'.format(NAME, VERSION, RPMRELEASE, TARGETDIST)
SHATYPE = 'sha256'

HERE = os.path.abspath(os.path.dirname(__file__))
#OUTDIR = os.path.join(HERE, 'builds/{}'.format(VERSION))

# these are temporary, should be options with defaults
BRANCH = get_var('branch', 'develop')


################################################################################
# PROJECT TASKS
# These tasks should be pretty much consistent for all qadevel projects, driven
# by the variables listed above
################################################################################


#def task_chainbuild():
#    """mockchain package using COPR repo to assist. Will fail if deps in repo are not up to date"""
#    tmpdir_prefix = '{}-{}'.format(NAME, datetime.utcnow().strftime('%Y%m%d%H%M%S'))
#    mockchain_command = 'mockchain -r {} --tmp_prefix={} -a {} {}/{}.src.rpm'.format(MOCKENV, tmpdir_prefix, COPRREPO, OUTDIR, NVR)
#    cp_command = 'cp /var/tmp/mock-chain-{}*/results/{}/{}/{}.{}.rpm {}/.'.format(tmpdir_prefix, MOCKENV, NVR, NVR, BUILDARCH, OUTDIR)
#
#    return {
#            'actions': [mockchain_command, cp_command],
#            'task_dep': ['buildsrpm'],
#            'targets': ['{}.rpm'.format(NVR)]
#            }
#
#def task_buildsrpm():
#    """build SRPM using mock, the latest git shapshot and current specfile details"""
#    mocksrpm_command = 'mock -r {} --buildsrpm --spec={} --sources={}'.format(MOCKENV, SPECFILE, OUTDIR)
#    cp_command = 'cp /var/lib/mock/{}/result/{}-{}-{}.{}.src.rpm {}/.'.format(MOCKENV, NAME, VERSION, RPMRELEASE, TARGETDIST, OUTDIR)
#
#    return {
#            'actions': [mocksrpm_command, cp_command],
#            'task_dep': ['snapshot'],
#            'targets': ['{}.src.rpm'.format(NVR)]
#            }
#
#def task_snapshot():
#    """Take snapshot of git specified git branch"""
#
#    return {
#            'actions': [(create_folder, [OUTDIR]),
#                        'git archive {} --prefix={}/ | gzip -c9 > {}.tar.gz'.format(BRANCH, TARGETNAME, os.path.join(OUTDIR,TARGETNAME))],
#            'targets': ['{}.tar.gz'.format(TARGETNAME)],
#            }
#
#def task_checksum():
#    """Generate checksum of git snapshot"""
#    sha_command = '{shatype}sum {outdir}/{target}.tar.gz > {outdir}/{target}.tar.gz.{shatype}'.format(shatype=SHATYPE, target=TARGETNAME, outdir=OUTDIR)
#    return {
#            'actions': [sha_command],
#            'task_dep': ['snapshot'],
#            'targets': ['{}.tar.gz.{}'.format(TARGETNAME, SHATYPE)]
#            }

def get_short_revision(project):
    # don't bother parsing revision if the project dir doesn't exist
    if not os.path.isdir('{project}/{project}'.format(project=project)):
        return
    gitrev_command = ['git',
                      '-C',
                      '{project}/{project}'.format(project=project),
                      'rev-parse',
                      '--branches={}'.format(GITBRANCH),
                      '--short',
                      'HEAD']
    raw_output = subprocess.check_output(gitrev_command).split()[0].lstrip('"')
    return raw_output

def get_long_revision(project):
    if not os.path.isdir('{project}/{project}'.format(project=project)):
        return

    gitrev_command = ['git',
                      '-C',
                      '{project}/{project}'.format(project=project),
                      'rev-parse',
                      '--branches={}'.format(GITBRANCH),
                      'HEAD']
    raw_output = subprocess.check_output(gitrev_command).split()[0].lstrip('"')
    return raw_output

def get_newest_rpm(project, srpm=True):
    if srpm:
        pathspec = '{}/*.src.rpm'
    else:
        pathspec = '{}/*.noarch.rpm'

    filenames = glob.glob(pathspec.format(project))

    newestfile = None
    newestctime = 0
    for name in filenames:
        stats = os.stat(name)
        if stats.st_ctime > newestctime:
            newestfile = name
            newestctime = stats.st_ctime

    return newestfile

def task_listfiles():
    for project in PROJECTS:
        newestrpm = get_newest_rpm(project, srpm=False)
        newestsrpm = get_newest_rpm(project, srpm=True)
        yield {'name': 'listfiles {}'.format(project),
                'actions': ['echo "newest rpm:  {}"'.format(newestrpm),
                            'echo "newest srpm: {}"'.format(newestsrpm)],
                'verbosity': 2
                }


def task_uploadsrpms():
    """upload srpms to fedorapeople"""
    for project in PROJECTS:
        srpm = get_newest_rpm(project)
        yield {'name': "building srpm for {}".format(project),
                'actions': ['scp {srpm} fedorapeople.org:~/public_html/packages/phabricator-dist/.'.format(mockenv=MOCKENV, project=project, srpm=srpm)],
                'verbosity': 2
              }

def task_build():
    """build rpms locally"""
    for project in PROJECTS:
        srpm = get_newest_rpm(project)
        yield {'name': "building srpm for {}".format(project),
               'actions': ['mock -r {mockenv} --rebuild {srpm}'.format(mockenv=MOCKENV, srpm=srpm),
                           'cp /var/lib/mock/{mockenv}/result/{project}*.noarch.rpm {project}/.'.format(mockenv=MOCKENV, project=project)],
                'verbosity': 2
              }


def task__copyrpmstorepo():
    """Copy latest local builds to local repo"""
    for project in PROJECTS:
        rpm = get_newest_rpm(project, srpm=False)
        yield {'name': "copying rpm to local repo for {}".format(project),
               'actions': ['cp {rpm} {repodir}/.'.format(rpm=rpm, repodir=LOCALREPO)],
               'verbosity': 2
              }


def task__repodir():
    """Create local repo dir if it doesn't already exist"""
    return {'actions': [(create_folder, [LOCALREPO])]}


def task_createrepo():
    """Create a local repo including the latest local builds"""
    return {'actions': ['createrepo {repodir}'.format(repodir=LOCALREPO)],
            'task_dep': ['_repodir', '_copyrpmstorepo']
            }

def task_buildsrpm():
    """build srpms with mock"""
    for project in PROJECTS:
        specfile = '{project}/{project}.spec'.format(project=project)
        yield {'name': "building srpm for {}".format(project),
               'actions': ['mock -r {mockenv} --buildsrpm --spec={specfile} --sources={project}'.format(mockenv=MOCKENV, specfile=specfile, project=project),
                           'cp /var/lib/mock/{mockenv}/result/{project}*.src.rpm {project}/.'.format(mockenv=MOCKENV, project=project)],
                'verbosity': 2
                }

def task_updatespec():
    """update specfiles for projects"""
    now = datetime.now()
    short_date = now.strftime('%Y%m%d')
    full_date = now.strftime('%a %b %d %Y')
    author = "Tim Flink <tflink@fedoraproject.org>"
    for project in PROJECTS:
        specfile = '{project}/{project}.spec'.format(project=project)
        release_num = get_rpmrelease(specfile)
        short_rev = get_short_revision(project)
        long_rev = get_long_revision(project)
        version = '{date}.git{rev}-{release}'.format(date=short_date, rev=short_rev, release=release_num)

        yield {'name': "updating {}".format(project),
               'actions': ['sed -i "s/%%define date [0-9]\+/%%define date {date}/" {specfile}'.format(date=short_date, specfile=specfile),
                'sed -i "s/%%define git_short_version_hash [0-9a-f]\+$/%%define git_short_version_hash {rev}/" {specfile}'.format(rev=short_rev, specfile=specfile),
                'sed -i "s/%%define git_full_version_hash [0-9a-f]\+$/%%define git_full_version_hash {rev}/" {specfile}'.format(rev=long_rev, specfile=specfile),
                'sed -i "s/%%changelog$/%%changelog\\n* {fulldate} {author} - {version}\\n- updating to latest git\\n/" {specfile}'.format(fulldate=full_date, author=author, version=version, specfile=specfile)],
                'verbosity': 2
                }

def task_snapshot():
    """make git snapshots for the projects"""
    for project in PROJECTS:
        current_rev = get_short_revision(project)
        yield {'name': "snapshotting {}".format(project),
                'actions': ['if [ ! -e "{project}//{rev}.tar.gz" ]; then wget -P {project} {repo}/archive/{rev}.tar.gz; fi'.format(project=project, repo=REPOS[project], rev=current_rev)],
                'verbosity': 2
                }



def task_update():
    """update git checkouts, assume that they haven't been changed"""
    for project in PROJECTS:
        yield {'name': "updating {}".format(project),
                'actions': ['pushd {project}/{project}; git fetch; git rebase origin/{branch}; popd'.format(project=project, repo=REPOS[project], branch=GITBRANCH)],
                'verbosity': 2
                }


def task_init():
    """init projects and local yumrepo"""
    for project in PROJECTS:
        yield {'name': "initializing {}".format(project),
                'actions': ['if [ ! -e "{project}/{project}" ]; then git clone --branch {branch} {repo}.git {project}/{project}; fi'.format(project=project, repo=REPOS[project], branch=GITBRANCH)],
                'verbosity': 2
                }

