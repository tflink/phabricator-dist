======================================
Fedora Packaging Notes for Phabricator
======================================

This document details the work needed in order to get phabricator, arcanist
and libphutil accepted as packages in Fedora (and possible EPEL 6/7).

The work is going to be mostly around unbundling the various PHP bits that are
currently included upstream. So far, we've found at least one of those deps
which has diverged from upstream it at least some incompatible ways.

The phases that I anticiapte going through are:

  # dig through the externals and figure out which have diverged from upstream

  # try to figure out why modified, bundled deps were changed and how much work
    it'd be to get a patch accepted to the dep upstreams or phab upstream

  # resolve the bundled lib issues

  # rework the pathing - I don't think that the current "stuff everything in 
    /usr/share" method is going to pass review

  # submit for review

  # ...

  # profit!


Other possible thoughts:

 * chat with PHP folks, see if they'd be willing to proofread some of this to
   make sure that we're doing sane things

 * figure out if there are better ways to run things than just apache+mod_php


libphutil
=========

Externals
---------

  * jsonlint

Potential Issues
----------------

  * Might need to deal with flex and building xphast

    * not sure what this meant when I wrote it - reinvestigate

  * jsonlint is modified from upstream.

     * with recent changes to upstream, it doesn't seem too different and other
       than the name changes, might be patchable to use vanilla upstream


Arcanist
========

Externals
---------

Arcanist has no externals

Potential Issues
----------------

  * pep8 (handle the local copy that arcanist appears to be expecting)

    * I don't know if this is still true, needs re-investigation



phabricator
===========

Externals
---------

amazon-ses
##########

  * interface to AWS simple email service

  * current upstream: https://github.com/daniel-zahariev/php-aws-ses

    * currently at 0.8.4

    * BSD 2-Clause License

  * this bundle appears to be upstream 0.8.1 with all the different php files
    concatenated into a single ``ses.php``

diff_match_patch
################

  * php port of Neil Frasers diff_match_patch - "Diff, Match and Patch
    libraries for Plain Text"

  * pre-port's upstream appears to be https://code.google.com/p/google-diff-match-patch/

        * pre-port is APL2.0

  * current upstream appeas to be https://github.com/nuxodin/diff_match_patch-php

  * php port is APL2.0

  * does not appear to be packaged in Fedora

  * looks like pretty much an older, vanilla upstream

      - the diff appears to be the one commit that has happened upstream
        since it was bundled

      - no changes in ``externals/diff_match_patch`` since it was first added
        in ~ Dec 2013 ( 4c143ad - Phragment v0 )

httpful
#######

  * simple, chainable, readable PHP library intended to make speaking HTTP sane.

  * http://phphttpclient.com/

      - https://github.com/nategood/httpful

  * MIT license

  * PHP 5.3+, last commit in master is May, 2015

  * does not appear to be packaged in Fedora

  * appears to be a plain vanilla upstream bundle, added for the balanced
    payments API , hasn't changed in phabricator since initial bundle
    ~ April, 2013 (2378678 - Add Balanced Payments API)

JsShrink
########

  * Remove spaces and comments from JavaScript code (available in both PHP and
    JavaScript)

  * https://github.com/vrana/JsShrink/

  * APL 2.0

  * only the php parts of upstream

  * appears to be 100% vanilla, updated once with changes from upstream

mimemailparser
##############

  * Strives to create a fast and efficient PHP Mime Mail Parser Class using
    PHP's MailParse Extension.

      * Requires the MBstring extension, MailParse PECL extension
  * Original upstream https://code.google.com/p/php-mime-mail-parser/

     * current fork appears to be: https://github.com/php-mime-mail-parser/php-mime-mail-parser

     * fork is for PHP 5.4+

  * MIT License

  * appears to have diverged from upstream for bugfixes

phpmailer
#########

This looks like it's going to require some changes upstream in phabricator
or a bundling exception from FPC - I don't see how the bits in ``externals/``
could be compatible with the latest Fedora package


  * full-featured email creation and transfer class for PHP

  * original upstream is http://phpmailer.worxware.com/ (this appears to be
    where the bundled code is from, too)

    * maintenance has since changed over to https://github.com/PHPMailer/PHPMailer

    * first bundled ~ January, 2011

    * There's mention of merging 2 forks together, the workxware one and something
      else

  * decent number of bugfixes in the bundle, does not appear to be updated from
    new upstream - I suspect they're now incompatible

  * LGPL 2.1, not sure if it's 2.1+ or not

  * packaged in Fedora

    * php-PHPMailer

    * 5.2.9 in fc21 (upstream is at 5.2.10)

    * listed as LGPL2+

    * FWIW, appears to list the wrong URL for upstream (the older one which
      says that maintenance has been transferred)

phpqrcode
#########

  * library for generating QR codes, based on libqrencode C library.

  * http://phpqrcode.sourceforge.net/

     * last release 1.1.4, 2013-05-20

  * LGPL3+

  * appears to be a plain vanilla bundle

recaptcha
#########

  * version 1.0 for google's recaptcha

  * I'm not even sure this is used any more, might be able to just get rid of
    it

  * appears to have been bundled in January, 2011 and not touched since

restful
#######

 * Library for writing RESTful PHP clients

 * https://github.com/balanced/restful

 * MIT License

 * Appears to have been bundled for balanced payments API and not touched
   since then, ~ Apr 2013 ( 2378678 - Add Balanced Payments API )

s3
##

  * Standalone Amazon S3 client for PHP using CURL that does not require PEAR

  * https://github.com/tpyo/amazon-s3-php-class

    * latest upstream is 0.5.1

    * bundled is listed as 0.5.0-dev

  * Looks like 2-clause BSD

  * appears to be a vanilla bundle - updated once since bundling in ~ June 2013

skins
#####

  * I'm not sure what this is, might be the beginnings of supporting skins for
    phabricator

  * doesn't appear to be bundled? if it is, there's no reference that I could
    find or license

  * hasn't been touched since ~ June 2013, suspect it's not being used anymore

stripe-php
##########

  * php library for interfacing with stripe

  * https://github.com/stripe/stripe-php

     * latest upstream release - 2.2.0

     * bundled release - 1.16.0

  * MIT licensea

  * appears to be a vanilla bundle, updated once ~ june 2014

twilio-php
##########

  * library for interacting with the twilio API

  * https://github.com/twilio/twilio-php

    * latest upstream release - 4.0.4 (may 6, 2015)

    * bundled release - 3.12.4 (January 30, 2014)a

  * MIT licensed

  * appears to be a vanilla bundle, one change since import that appears to be
    changing file metadata (removing +x from files)

     * baa6441 - Remove some needless +x flags


wepay
#####

  * library for interfacing with wepay for payments

  * https://github.com/wepay/PHP-SDK/

     * latest upstream release - 0.2.6 (Apr 22, 2015)

     * bundled release - 0.1.4 (~ October, 2013)

  *  bundle is MIT licensed

  * upstream changed to APL 2.0 with their latest release

  * appears to be a vanilla bundle, just siginficantly older than upstream

wordlist
########

  * a list of common passwords for blacklisting

  * http://www.openwall.com/wordlists/

  * license is not any standard SW license, doesn't seem to be compatible with
    Fedora


xhprof
######

  * XHProf is a function-level hierarchical profiler for PHP and has a simple
    HTML based user interface

  * http://pecl.php.net/package/xhprof

    * https://github.com/phacility/xhprof is a fork?

  * APL 2.0

  * appears to have been updated once since initial bundle in ~ January 2011

  * I wonder if it's even being used anymore


Potential Problems
------------------

  * phpmailer

    * see notes above - won't be a simple unbundle :(

  * JsShrink
     - dual licensed under APL2.0 and GPL2.0
     - not sure if that's OK for fedora packages

  * recaptcha
     - I know that we don't want to use stuff like recaptcha in fedora, not sure
       if there are any issues in packaging something like this, though

  * skins
     - not sure exactly what this is, appears to be skins for phab?

  * wordlist
     - Looks to have a Fedora-incompatible license
     - it might be apl2 incompatible, too. not sure

Packaged in Fedora
------------------

  * phpmailer

  * xhprof

