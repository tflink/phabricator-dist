%define git_short_version_hash d414e44
%define git_full_version_hash d414e4436e1c8779634458c23ab8880cf613312f
%define date 20160407

Summary: A collection of PHP utility classes
Name: libphutil
Version: %{date}.git%{git_short_version_hash}
Release: 1%{?dist}
License: ASL 2.0
URL: http://www.phabricator.com/docs/libphutil/
Source0: https://github.com/phacility/libphutil/archive/%{git_short_version_hash}.tar.gz
BuildArch: noarch

Requires: php-common >= 5

%description
A collection of PHP utility classes used with phabricator


%prep
%setup -q -n libphutil-%{git_full_version_hash}


%build


%install
# copy libphutil to the buildroot
mkdir -p %{buildroot}%{_datadir}/%{name}
cp -a * %{buildroot}%{_datadir}/%{name}/



%files
%doc %{_datadir}/%{name}/README.md
%doc %{_datadir}/%{name}/LICENSE
%doc %{_datadir}/%{name}/NOTICE
%{_datadir}/%{name}/resources
%{_datadir}/%{name}/scripts
%{_datadir}/%{name}/src
%{_datadir}/%{name}/support
%{_datadir}/%{name}/externals/includes/README
%{_datadir}/%{name}/externals/jsonlint
%{_datadir}/%{name}/bin/aws-s3



%changelog
* Thu Apr 07 2016 Tim Flink <tflink@fedoraproject.org> - 20160407.gitd414e44-1
- updating to latest git

* Thu Mar 10 2016 Tim Flink <tflink@fedoraproject.org> - 20160310.git76425ea-1
- updating to latest git

* Tue Feb 23 2016 Tim Flink <tflink@fedoraproject.org> - 20160223.git9c472e7-1
- updating to latest git

* Wed Feb 17 2016 Tim Flink <tflink@fedoraproject.org> - 20160217.git9c472e7-1
- updating to latest git

* Tue Feb 09 2016 Tim Flink <tflink@fedoraproject.org> - 20160209.git9c472e7-1
- updating to latest git
- adding bin/aws-s3 to files

* Fri Jan 29 2016 Tim Flink <tflink@fedoraproject.org> - 20160129.git9c472e7-1
- updating to latest git

* Fri Nov 06 2015 Tim Flink <tflink@fedoraproject.org> - 20151106.gita1e8e2c-1
- updating to latest git

* Tue Sep 22 2015 Tim Flink <tflink@fedoraproject.org> - 20150922.git0c7b526-1
- updating to latest git

* Wed Sep 02 2015 Tim Flink <tflink@fedoraproject.org> - 20150902.git161e36f-1
- changing to stable branch upstream
- artificial bump of date in name to differentiate

* Wed Sep 02 2015 Tim Flink <tflink@fedoraproject.org> - 20150902.gitece08d9-2
- removing patch as it's no longer needed

* Wed Sep 02 2015 Tim Flink <tflink@fedoraproject.org> - 20150902.gitece08d9-1
- updating to latest git

* Fri Aug 21 2015 Tim Flink <tflink@fedoraproject.org> - 20150821.gite509fc3-1
- updating to latest git

* Tue Aug 11 2015 Tim Flink <tflink@fedoraproject.org> - 20150811.git6c01180-1
- updating to latest git

* Mon Jul 27 2015 Tim Flink <tflink@fedoraproject.org> - 20150727.gitaa6cd8f-1
- updating to latest git

* Mon Jul 20 2015 Tim Flink <tflink@fedoraproject.org> - 20150720.gitaa6cd8f-1
- updating to latest git

* Thu Jul 16 2015 Tim Flink <tflink@fedoraproject.org> - 20150716.gitaa6cd8f-1
- updating to latest git

* Thu Jul 09 2015 Tim Flink <tflink@fedoraproject.org> - 20151909.gitaa6cd8f-1
- updating to latest git

* Wed May 13 2015 Tim Flink <tflink@fedoraproject.org> - 20150211.git13899e4-3
- re-removing JsonLint link after botched update to specfile

* Wed May 13 2015 Tim Flink <tflink@fedoraproject.org> - 20150211.git13899e4-2
- updating for changed filenames

* Wed May 13 2015 Tim Flink <tflink@fedoraproject.org> - 20150211.git13899e4-1
- Updating to latest upstream git

* Wed Feb 25 2015 Tim Flink <tflink@fedoraproject.org> - 20150211.git9e0ea2c-2
- un-removing bundled jsonlint because the bundled version isn't compatible with upstream

* Wed Feb 11 2015 Tim Flink <tflink@fedoraproject.org> - 20150211.git9e0ea2c-1
- Updating to latest upstream git

* Sat Jan 17 2015 Vladimir Rusinov <vladimir@greenmice.info> - 20141204.git549aa1b-2
- remove bundled jsonlint

* Thu Dec 04 2014 Tim Flink <tflink@fedoraproject.org> - 20141204.git549aa1b-1
- updating to latest upstream git

* Tue Sep 02 2014 Tim Flink <tflink@fedoraproject.org> - 20140902.git2de6440-1
- updating to latest upstream git

* Mon Jul 21 2014 Tim Flink <tflink@fedoraproject.org> - 20140721.gitefc2cc5-1
- updating to latest upstream git

* Sun May 18 2014 Tim Flink <tflink@fedoraproject.org> - 20140518.git1add454-1
- updating to latest upstream git

* Thu Jan 23 2014 Tim Flink <tflink@fedoraproject.org> - 20140123.git86d651f-1
- updating to latest git

* Tue Dec 10 2013 Tim Flink <tflink@fedoraproject.org> - 20131210.git69490c5-1
- updating to latest git

* Wed Oct 30 2013 Tim Flink <tflink@fedoraproject.org> - 20131030.gitba9c942-1
- Initial package
