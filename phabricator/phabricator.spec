%define git_short_version_hash fea2389
%define git_full_version_hash fea2389066edf3ad0a7547ae12d8e988428a4f5c
%define date 20160407

Summary: Open software engineering platform and fun adventure game
Name: phabricator
Version: %{date}.git%{git_short_version_hash}
Release: 1%{?dist}
License: ASL 2.0
URL: http://www.phabricator.org/
Source0: https://github.com/phacility/phabricator/archive/%{git_short_version_hash}.tar.gz
Source1: phabricator-phd.service
BuildArch: noarch

Requires: arcanist
Requires: libphutil
Requires: git
Requires: php
Requires: php-cli
Requires: php-common >= 5
Requires: php-mysql
Requires: php-process
Requires: php-devel
Requires: php-gd
Requires: php-mbstring
%if 0%{?rhel}
Requires: php-pecl-json
Requires: php-pecl-apc
%else
Requires: php-pecl-apcu
%endif
Requires(pre): /usr/sbin/useradd, /usr/bin/getent

# Suggests implemented in RPM 4.12+ which is only included in F21 and hopefully
# will be included in next RHEL.
%if 0%{?fedora} >= 21
Suggests: httpd
Suggests: mod_ssl
Suggests: mariadb-server
%else
%if 0%{?rhel} >= 8
Suggests: httpd
Suggests: mod_ssl
Suggests: mariadb-server
%endif
%endif

%description
Open software engineering platform and fun adventure game


%prep
%setup -q -n phabricator-%{git_full_version_hash}


%build


%install
# copy phabricator to the buildroot
mkdir -p %{buildroot}%{_datadir}/%{name}
cp -a * %{buildroot}%{_datadir}/%{name}/
# copy systemd unit file:
mkdir -p ${RPM_BUILD_ROOT}/usr/lib/systemd/system/
cp %{SOURCE1} ${RPM_BUILD_ROOT}/usr/lib/systemd/system/

%pre
/usr/bin/getent group phabricator > /dev/null || /usr/sbin/groupadd -r phabricator
/usr/bin/getent passwd phabricator > /dev/null || /usr/sbin/useradd -r -d /usr/share/phabricator -s /sbin/nologin -g phabricator phabricator

%files
%{_datadir}/%{name}
/usr/lib/systemd/system/phabricator-phd.service


%changelog
* Thu Apr 07 2016 Tim Flink <tflink@fedoraproject.org> - 20160407.gitfea2389-1
- updating to latest git

* Thu Mar 10 2016 Tim Flink <tflink@fedoraproject.org> - 20160310.git8b56e00-1
- updating to latest git

* Tue Feb 23 2016 Tim Flink <tflink@fedoraproject.org> - 20160223.gitad53db0-1
- updating to latest git

* Wed Feb 17 2016 Tim Flink <tflink@fedoraproject.org> - 20160217.git70c6791-1
- updating to latest git

* Tue Feb 09 2016 Tim Flink <tflink@fedoraproject.org> - 20160209.gitb3dd0fd-1
- updating to latest git

* Fri Jan 29 2016 Tim Flink <tflink@fedoraproject.org> - 20160129.git864d398-1
- updating to latest git

* Fri Nov 06 2015 Tim Flink <tflink@fedoraproject.org> - 20151106.git686895d-1
- updating to latest git

* Tue Sep 22 2015 Tim Flink <tflink@fedoraproject.org> - 20150922.git22e60f9-1
- updating to latest git

* Wed Sep 02 2015 Tim Flink <tflink@fedoraproject.org> - 20150903.git5125045-1
- changing to stable branch upstream
- artificial bump of date in name to differentiate

* Wed Sep 02 2015 Tim Flink <tflink@fedoraproject.org> - 20150902.gita13db0a-1
- updating to latest git

* Fri Aug 21 2015 Tim Flink <tflink@fedoraproject.org> - 20150821.gitd825aae-1
- updating to latest git

* Tue Aug 11 2015 Tim Flink <tflink@fedoraproject.org> - 20150811.git53ffaaa-1
- updating to latest git

* Mon Jul 27 2015 Tim Flink <tflink@fedoraproject.org> - 20150727.git84049e6-1
- updating to latest git

* Mon Jul 20 2015 Tim Flink <tflink@fedoraproject.org> - 20150720.gitc3e16e2-1
- updating to latest git

* Thu Jul 16 2015 Tim Flink <tflink@fedoraproject.org> - 20150716.gitae28130-1
- updating to latest git

* Thu Jul 09 2015 Tim Flink <tflink@fedoraproject.org> - 20150709.gite8f063d-1
- updating to latest git

* Wed May 13 2015 Tim Flink <tflink@fedoraproject.org> - 20150513.git20b7308-1
- updating to newest upstream git snapshot

* Wed Feb 11 2015 Tim Flink <tflink@fedoraproject.org> - 20150211.gita3f380a-1
- updating to newest upstream git snapshot

* Sun Jan 11 2015 Vladimir Rusinov <vladimir@greenmice.info> - 20141204.git2856c60-5
- create user 'phabricator'
- add phd systemd service

* Sun Jan 11 2015 Vladimir Rusinov <vladimir@greenmice.info> - 20141204.git2856c60-4
- add arcanist dependency

* Sun Jan 11 2015 Vladimir Rusinov <vladimir@greenmice.info> - 20141204.git2856c60-3
- rename mysql-server dependency to mariadb-server
- move mariadb-server dependency to suggests section

* Thu Jan 01 2015 Vladimir Rusinov <vladimir@greenmice.info> - 20141204.git2856c60-2
- move httpd and mod_ssl to Suggests section as they are not hard dependencies
  and can be ignored when using php-fpm or similar

* Thu Dec 04 2014 Tim Flink <tflink@fedoraproject.org> - 20141204.git2856c60-1
- updating to newest upstream git snapshot

* Tue Sep 02 2014 Tim Flink <tflink@fedoraproject.org> - 20140902.git6be8d65-1
- updating to newest upstream git snapshot

* Mon Jul 21 2014 Tim Flink <tflink@fedoraproject.org> - 20140721.git55fed95-1
- updating to newest upstream git snapshot

* Sun May 18 2014 Tim Flink <tflink@fedoraproject.org> - 20140518.git3a81f8c-1
- updating to newest upstream, removing custom patch

* Thu Jan 23 2014 Tim Flink <tflink@fedoraproject.org> - 20140123.git57f1a83-2
- updating custom patch to include search panel and move "projects" application to core

* Thu Jan 23 2014 Tim Flink <tflink@fedoraproject.org> - 20140123.git57f1a83-1
- updating to latest git
- adding custom patch to make landing root url publicly viewable

* Fri Dec 13 2013 Tim Flink <tflink@fedoraproject.org> - 20131210.git270c8d2-3
- removing php-pecl-json for fedora

* Fri Dec 13 2013 Tim Flink <tflink@fedoraproject.org> - 20131210.git270c8d2-2
- changing php-pecl-apc to php-pecl-apcu on fedora

* Tue Dec 10 2013 Tim Flink <tflink@fedoraproject.org> - 20131210.git270c8d2-1
- updating package to latest git head

* Wed Oct 30 2013 Tim Flink <tflink@fedoraproject.org> - 20131030.git2f7057a-1
- Initial package
